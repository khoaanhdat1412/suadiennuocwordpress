<?php
/*
 * Plugin Name: Address Section Contact Widget
 */

add_action('widgets_init', 'create_address_section_widget');

/**
 * Khoi tao widget
 */

function create_address_section_widget() {
    register_widget('Address_Section_Contact');
}

/**
 * Tao widget
 */

/**
 * Tao Widget
 * Class Gallery_Widget
 */
class Address_Section_Contact extends WP_Widget {
    function __construct()
    {
        parent::__construct(
            'address_section_contact_widget',
            'Address Sections Contact',
            array(
                'description' => 'This is a address section in Contact'
            )
        );
    }

    function form($instance)
    {
        $default = array(
            'stt' => '',
            'address' => ''
        );
        $instance = wp_parse_args($instance, $default);
        $stt = esc_attr($instance['stt']);
        $address = esc_attr($instance['address']);
        echo ('Stt: <input type="number" class="widefat" value="'. $stt .'" name="'. $this->get_field_name('stt') .'"/>');
        echo ('Địa chỉ: <input type="text" class="widefat" value="'. $address .'" name="'. $this->get_field_name('address') .'"/>');
    }

    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['address'] = $new_instance['address'];
        $instance['stt'] = $new_instance['stt'];
        return $instance;
    }

    function widget($args, $instance)
    {
        extract($args);
        $address = $instance['address'];
        $stt = $instance['stt'];
        echo '<p><span>Cơ Sở '. $stt .': </span> '. $address .'</p>';
    }
}