<?php
/*
 * Plugin Name: Email Section Contact Widget
 */

add_action('widgets_init', 'create_email_section_widget');

/**
 * Khoi tao widget
 */

function create_email_section_widget() {
    register_widget('Email_Section_Contact');
}

/**
 * Tao widget
 */

/**
 * Tao Widget
 * Class Email_Section_Contact
 */
class Email_Section_Contact extends WP_Widget {
    function __construct()
    {
        parent::__construct(
            'email_section_contact_widget',
            'Email Sections Contact',
            array(
                'description' => 'This is a email section in Contact'
            )
        );
    }

    function form($instance)
    {
        $default = array(
            'stt' => '',
            'email' => ''
        );
        $instance = wp_parse_args($instance, $default);
        $stt = esc_attr($instance['stt']);
        $email = esc_attr($instance['email']);
        echo ('Stt: <input type="number" class="widefat" value="'. $stt .'" name="'. $this->get_field_name('stt') .'"/>');
        echo ('Email: <input type="text" class="widefat" value="'. $email .'" name="'. $this->get_field_name('email') .'"/>');
    }

    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['email'] = $new_instance['email'];
        $instance['stt'] = $new_instance['stt'];
        return $instance;
    }

    function widget($args, $instance)
    {
        extract($args);
        $email = $instance['email'];
        $stt = $instance['stt'];
        echo '<p>'. $email .'</p>';
    }
}