<?php
/*
 * Plugin Name: Gallery Widget
 */

add_action('widgets_init', 'create_gallery_section_widget');

/**
 * Khoi tao widget
 */

function create_gallery_section_widget() {
    register_widget('Gallery_Widget');
}

/**
 * Tao widget
 */

/**
 * Tao Widget
 * Class Gallery_Widget
 */
class Gallery_Widget extends WP_Widget {
    function __construct()
    {
        parent::__construct(
            'gallery_section_widget',
            'Gallery Sections Homepage',
            array(
                'description' => 'This is a gallery section in Homepage'
            )
        );
    }

    function form($instance)
    {
        $default = array(
            'service_name' => '',
            'link_service' => ''
        );
        $instance = wp_parse_args($instance, $default);
        $serviceName = esc_attr($instance['service_name']);
        $linkService = esc_attr($instance['link_service']);
        echo ('Dịch Vụ: <input type="text" class="widefat" value="'. $serviceName .'" name="'. $this->get_field_name('service_name') .'"/>');
        echo ('Url Dịch Vụ: <input type="text" class="widefat" value="'. $linkService .'" name="'. $this->get_field_name('link_service') .'"/>');
    }

    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['service_name'] = $new_instance['service_name'];
        $instance['link_service'] = $new_instance['link_service'];
        return $instance;
    }

    function widget($args, $instance)
    {
        extract($args);
        $serviceName = $instance['service_name'];
        $linkService = $instance['link_service'];
        $image = get_field('image_gallery', 'widget_' . $args['widget_id']);

        echo '<div class="col-md-3 ftco-animate">';
        echo '<div class="work mb-4 img d-flex align-items-end" style="background-image: url('. $image['url'] .');">';
        echo    '<a class="icon image-popup d-flex justify-content-center align-items-center" href="'. $image['url'] .'">';
        echo        '<span class="fa fa-expand"></span>';
        echo    '</a>';
        echo    '<div class="desc w-100 px-4">';
        echo        '<div class="text w-100 mb-3">';
        echo            '<h2><a href="'. $linkService .'">'. $serviceName .'</a></h2>';
        echo        '</div>';
        echo    '</div>';
        echo '</div>';
        echo '</div>';
    }
}