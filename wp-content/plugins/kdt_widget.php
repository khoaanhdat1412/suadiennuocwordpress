<?php
/*
 * Plugin Name: KDT Widget
 */

add_action('widgets_init', 'create_kdt_widget');

/**
 * Khoi tao widget
 */

function create_kdt_widget() {
    register_widget('Kdt_Widget');
}

/**
 * Tao widget
 */

/**
 * Tao Widget
 * Class Kdt_Widget
 */
class Kdt_Widget extends WP_Widget {
    function __construct()
    {
        parent::__construct(
            'reason_widget',
            'Lý do Homepage',
            array(
                'description' => 'This is a reason section in Homepage'
            )
        );
    }

    function form($instance)
    {
        $default = array(
            'title' => '',
            'class_icon' => '',
            'content' => ''
        );
        $instance = wp_parse_args($instance, $default);
        $title = esc_attr($instance['title']);
        $classIcon = esc_attr($instance['class_icon']);
        $content = esc_attr($instance['content']);
        echo ('Tiêu đề: <input type="text" class="widefat" value="'. $title .'" name="'. $this->get_field_name('title') .'"/>');
        echo ('Class Icon: <input type="text" class="widefat" value="'. $classIcon .'" name="'. $this->get_field_name('class_icon') .'"/>');
        echo ('Nội dung: <textarea class="widefat" name="'. $this->get_field_name('content') .'">'. $content .'</textarea>');
    }

    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = $new_instance['title'];
        $instance['class_icon'] = $new_instance['class_icon'];
        $instance['content'] = $new_instance['content'];
        return $instance;
    }

    //hien thi widget ra ben ngoai
    function widget($args, $instance)
    {
        extract($args);
        $title = apply_filters('widget_title', $instance['title']);
        echo '<div class="col-md-4 services-2 w-100 d-flex">';
        echo '<div class="icon d-flex align-items-center justify-content-center">';
        echo '<span class="' . $instance['class_icon'] .'"></span>';
        echo '</div>';
        echo '<div class="text pl-3">';
        echo '<h4>'. $before_title.$title.$after_title .'</h4>';
        echo '<p>'. $instance['content'] .'</p>';
        echo '</div>';
        echo '</div>';
    }
}