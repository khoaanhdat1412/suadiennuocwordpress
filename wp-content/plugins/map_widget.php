<?php
/*
 * Plugin Name: Map Widget
 */

add_action('widgets_init', 'create_map_widget');

/**
 * Khoi tao widget
 */

function create_map_widget() {
    register_widget('Map_Widget');
}

/**
 * Tao widget
 */

/**
 * Tao Widget
 * Class Map_Widget
 */
class Map_Widget extends WP_Widget {
    function __construct()
    {
        parent::__construct(
            'map_widget',
            'Map Widget',
            array(
                'description' => 'This is a map section'
            )
        );
    }

    function form($instance)
    {
        $default = array(
            'title' => '',
            'address' => '',
            'url_address' => '',
            'phone_number' => '',
            'href_phone' => ''
        );
        $instance = wp_parse_args($instance, $default);
        $title = esc_attr($instance['title']);
        $address = esc_attr($instance['address']);
        $urlAddress = esc_attr($instance['url_address']);
        $phoneNumber = esc_attr($instance['phone_number']);
        $hrefPhone = esc_attr($instance['href_phone']);
        echo ('Tiêu đề: <input type="text" class="widefat" value="'. $title .'" name="'. $this->get_field_name('title') .'"/>');
        echo ('Địa chỉ: <input type="text" class="widefat" value="'. $address .'" name="'. $this->get_field_name('address') .'"/>');
        echo ('Link địa chỉ: <input type="text" class="widefat" value="'. $urlAddress .'" name="'. $this->get_field_name('url_address') .'"/>');
        echo ('Điện thoại: <input type="text" class="widefat" value="'. $phoneNumber .'" name="'. $this->get_field_name('phone_number') .'"/>');
        echo ('Href điện thoại: <input type="text" class="widefat" value="'. $hrefPhone .'" name="'. $this->get_field_name('href_phone') .'"/>');
    }

    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = $new_instance['title'];
        $instance['address'] = $new_instance['address'];
        $instance['url_address'] = $new_instance['url_address'];
        $instance['phone_number'] = $new_instance['phone_number'];
        $instance['href_phone'] = $new_instance['href_phone'];
        return $instance;
    }

    function widget($args, $instance)
    {
        extract($args);
        $title = apply_filters('widget_title', $instance['title']);
        echo '<div class="col-md-4 ftco-animate bg-white map-item">';
        echo '<div class="title-background">';
        echo '<span>'. $title .'</span>';
        echo '</div>';
        echo '<div class="map-content">';
        echo '<p><span class="fa fa-map-marker"></span> <a rel="nofollow" target="_blank" href="'. $instance['url_address'] .'">'. $instance['address'] .'</a></p>';
        echo '<p><span class="fa fa-phone"></span><a class="phone-number" href="tel: '. $instance['href_phone'] .'">'. $instance['phone_number'] .'</a></p>';
        echo '</div>';
        echo '</div>';
    }
}