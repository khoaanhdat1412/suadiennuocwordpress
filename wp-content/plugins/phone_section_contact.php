<?php
/*
 * Plugin Name: Phone Section Contact Widget
 */

add_action('widgets_init', 'create_phone_section_widget');

/**
 * Khoi tao widget
 */

function create_phone_section_widget() {
    register_widget('Phone_Section_Contact');
}

/**
 * Tao widget
 */

/**
 * Tao Widget
 * Class Phone_Section_Contact
 */
class Phone_Section_Contact extends WP_Widget {
    function __construct()
    {
        parent::__construct(
            'phone_section_contact_widget',
            'Phone Sections Contact',
            array(
                'description' => 'This is a phone section in Contact'
            )
        );
    }

    function form($instance)
    {
        $default = array(
            'stt' => '',
            'phone' => ''
        );
        $instance = wp_parse_args($instance, $default);
        $stt = esc_attr($instance['stt']);
        $phone = esc_attr($instance['phone']);
        echo ('Stt: <input type="number" class="widefat" value="'. $stt .'" name="'. $this->get_field_name('stt') .'"/>');
        echo ('Điện thoại: <input type="text" class="widefat" value="'. $phone .'" name="'. $this->get_field_name('phone') .'"/>');
    }

    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['phone'] = $new_instance['phone'];
        $instance['stt'] = $new_instance['stt'];
        return $instance;
    }

    function widget($args, $instance)
    {
        extract($args);
        $phone = $instance['phone'];
        $stt = $instance['stt'];
        echo '<p><span> Điện thoại '. $stt .': </span> '. $phone .'</p>';
    }
}