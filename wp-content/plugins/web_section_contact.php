<?php
/*
 * Plugin Name: Web Section Contact Widget
 */

add_action('widgets_init', 'create_web_section_widget');

/**
 * Khoi tao widget
 */

function create_web_section_widget() {
    register_widget('Web_Section_Contact');
}

/**
 * Tao widget
 */

/**
 * Tao Widget
 * Class Web_Section_Contact
 */
class Web_Section_Contact extends WP_Widget {
    function __construct()
    {
        parent::__construct(
            'web_section_contact_widget',
            'Web Sections Contact',
            array(
                'description' => 'This is a web section in Contact'
            )
        );
    }

    function form($instance)
    {
        $default = array(
            'stt' => '',
            'web' => ''
        );
        $instance = wp_parse_args($instance, $default);
        $stt = esc_attr($instance['stt']);
        $web = esc_attr($instance['web']);
        echo ('Stt: <input type="number" class="widefat" value="'. $stt .'" name="'. $this->get_field_name('stt') .'"/>');
        echo ('Website: <input type="text" class="widefat" value="'. $web .'" name="'. $this->get_field_name('web') .'"/>');
    }

    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['web'] = $new_instance['web'];
        $instance['stt'] = $new_instance['stt'];
        return $instance;
    }

    function widget($args, $instance)
    {
        extract($args);
        $web = $instance['web'];
        $stt = $instance['stt'];
        echo '<a target="_blank" href="https://'. $web .'"><p><span> Website '. $stt .': </span> '. $web .'</p></a>';
    }
}