<div class="hero-wrap" style="background-image: url('<?php echo __(get_theme_mod('banner_homepage_image')) ?>');" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-end" data-scrollax-parent="true">
            <div class="banner-title col-md-6 ftco-animate">
                <h1 class="mb-4">
                    <?php echo __(get_theme_mod('banner_homepage_text_1')) ?> <br/>
                    <span><?php echo __( get_theme_mod('banner_homepage_text_2_1')) ?></span>
                    <span><?php echo __( get_theme_mod('banner_homepage_text_2_2')) ?></span>
                    <?php echo __(get_theme_mod('banner_homepage_text_3')) ?>
                </h1>
                <p><a href="/<?php echo __(get_theme_mod('banner_homepage_link_button')) ?>" class="btn btn-orange mr-md-4 py-3 px-4"><?php echo __(get_theme_mod('banner_homepage_text_button')) ?> <span
                            class="ion-ios-arrow-forward"></span></a></p>
            </div>
        </div>
    </div>
</div>