<?php get_header(); ?>
    <?php
        $category = get_queried_object();
        $image = get_field('category_banner_image', 'category_'. $category->term_id);
    ?>
    <section class="hero-wrap hero-wrap-2" style="background-image: url('<?= $image['url'] ?>');"
             data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-end">
                <div class="col-md-9 ftco-animate pb-5">
                    <p class="breadcrumbs mb-2"><span class="mr-2"><a href="<?php bloginfo('url'); ?>"><?= __('Trang Chủ') ?> <i
                                    class="ion-ios-arrow-forward"></i></a></span> <span><?= single_cat_title( '', false ) ?> <i
                                class="ion-ios-arrow-forward"></i></span></p>
                    <h1 class="mb-0 bread"><?= single_cat_title( '', false ) ?></h1>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section ftco-degree-bg list-blog">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 ftco-animate">
                    <div class="container">
                        <div class="row d-flex">
                            <?php if (have_posts()) : ?>
                                <?php while (have_posts()) : the_post(); ?>
                                    <div class="col-md-6 d-flex ftco-animate">
                                        <div class="blog-entry align-self-stretch">
                                            <a href="<?php the_permalink(); ?>" class="block-20 rounded"
                                               style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id(get_the_id()) );?>');">
                                            </a>
                                            <div class="text p-4">
                                                <div class="meta mb-2">
                                                    <div><a href="<?php the_permalink(); ?>"><?= get_the_date() ?></a></div>
                                                    <div><a href="<?php the_permalink(); ?>"><?php the_author(); ?></a></div>
                                                    <div>
                                                        <a href="<?php the_permalink(); ?>" class="meta-chat">
                                                            <span class="fa fa-comment"></span>
                                                            <?php echo get_comments_number(); echo __(' bình luận'); ?>
                                                        </a>
                                                    </div>
                                                </div>
                                                <h3 class="heading"><a href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a></h3>
                                            </div>
                                        </div>
                                    </div>
                                <?php endwhile;?>
                            <?php endif; ?>
                        </div>

                        <div class="row mt-5">
                            <div class="col text-center">
                                <div class="block-27 pagination-cat">
                                    <ul>
                                        <?php if(paginate_links()!='') {?>
                                            <?php
                                            global $wp_query;
                                            $big = 999999999;
                                            echo paginate_links( array(
                                                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                                'format' => '?paged=%#%',
                                                'prev_text'    => __('<'),
                                                'next_text'    => __('>'),
                                                'current' => max( 1, get_query_var('paged') ),
                                                'total' => $wp_query->max_num_pages
                                            ) );
                                            ?>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- .col-md-8 -->

                <?php get_sidebar(); ?>
            </div>
        </div>
    </section> <!-- .section -->

<?php get_footer(); ?>