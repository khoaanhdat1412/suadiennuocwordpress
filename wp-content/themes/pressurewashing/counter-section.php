<section class="ftco-counter" id="section-counter">
    <div class="container">
        <div class="row">
            <div class="col-md-4 mb-5 mb-md-0 text-center text-md-left">
                <h2 class="font-weight-bold" style="color: #fff; font-size: 20px;"><?php echo __(get_theme_mod('count_homepage_text_main')); ?> </h2>
                <a href="tel:<?php echo __(get_theme_mod('count_homepage_phone')); ?>" class="btn btn-white btn-outline-white"><?php echo __(get_theme_mod('count_homepage_text_button_main')); ?></a>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-6 col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                        <div class="block-18 text-center">
                            <div class="text">
                                <strong class="number" data-number="<?php echo get_theme_mod('count_homepage_fake_number_1'); ?>">0</strong>
                            </div>
                            <div class="text">
                                <span><?php echo __(get_theme_mod('count_homepage_fake_text_1')); ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                        <div class="block-18 text-center">
                            <div class="text">
                                <strong class="number" data-number="<?php echo get_theme_mod('count_homepage_fake_number_2'); ?>">0</strong>
                            </div>
                            <div class="text">
                                <span><?php echo __(get_theme_mod('count_homepage_fake_text_2')); ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                        <div class="block-18 text-center">
                            <div class="text">
                                <strong class="number" data-number="<?php echo get_theme_mod('count_homepage_fake_number_3'); ?>">0</strong>
                            </div>
                            <div class="text">
                                <span><?php echo __(get_theme_mod('count_homepage_fake_text_3')); ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                        <div class="block-18 text-center">
                            <div class="text">
                                <strong class="number" data-number="<?php echo get_theme_mod('count_homepage_fake_number_4'); ?>">0</strong>
                            </div>
                            <div class="text">
                                <span><?php echo __(get_theme_mod('count_homepage_fake_text_4')); ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>