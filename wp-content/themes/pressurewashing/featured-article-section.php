<section class="feature-article bg-light feature-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7 heading-section text-center ftco-animate">
                <h2><?= __('Bài Viết Nổi Bật') ?></h2>
            </div>
        </div>
        <div class="row d-flex">

            <?php
            $args = array(
                'post_status' => 'publish', // Chỉ lấy những bài viết được publish
                'post_type' => 'post', // Lấy những bài viết thuộc post, nếu lấy những bài trong 'trang' thì để là page
                'showposts' => 3, // số lượng bài viết
                'cat' => 9,
            );
            ?>
            <?php $getposts = new WP_query($args); ?>
            <?php global $wp_query; $wp_query->in_the_loop = true; ?>
            <?php while ($getposts->have_posts()) : $getposts->the_post(); ?>
                <div class="col-md-4 d-flex ftco-animate">
                    <div class="blog-entry align-self-stretch">
                        <a href="<?php the_permalink(); ?>" class="block-20 rounded"
                           style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id(get_the_id()) );?>');">
                        </a>
                        <div class="text p-4">
                            <div class="meta mb-2">
                                <div><a href="<?php the_permalink(); ?>"><?= get_the_date() ?></a></div>
                                <div><a href="<?php the_permalink(); ?>"><?php the_author(); ?></a></div>
                                <div>
                                    <a href="<?php the_permalink(); ?>" class="meta-chat">
                                        <span class="fa fa-comment"></span> <?= get_comments_number(); ?>
                                    </a>
                                </div>
                            </div>
                            <h3 class="heading">
                                <a href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a>
                            </h3>
                        </div>
                    </div>
                </div>
            <?php endwhile; wp_reset_postdata(); ?>

        </div>
    </div>
</section>