<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-3 mb-4 mb-md-0">
                <h2 class="footer-heading"><?php echo get_theme_mod('footer_column_1_title'); ?></h2>
                <p><?php echo get_theme_mod('footer_column_1_description'); ?></p>
                <ul class="ftco-footer-social p-0">
                    <li class="ftco-animate"><a target="_blank" href="<?php echo get_theme_mod('footer_column_1_facebook'); ?>" data-toggle="tooltip" data-placement="top"
                                                title="Facebook"><span class="fa fa-facebook"></span></a></li>
                    <li class="ftco-animate"><a target="_blank" href="<?php echo get_theme_mod('footer_column_1_instagram'); ?>" data-toggle="tooltip" data-placement="top"
                                                title="Instagram"><span class="fa fa-instagram"></span></a></li>
                </ul>
            </div>
            <div class="new-blog col-md-6 col-lg-3 mb-4 mb-md-0">
                <h2 class="footer-heading">Bài Viết Mới Nhất</h2>
                <?php
                $args = array(
                    'post_status' => 'publish', // Chỉ lấy những bài viết được publish
                    'post_type' => 'post', // Lấy những bài viết thuộc post, nếu lấy những bài trong 'trang' thì để là page
                    'showposts' => 1, // số lượng bài viết
                );
                ?>
                <?php $getposts = new WP_query($args); ?>
                <?php global $wp_query; $wp_query->in_the_loop = true; ?>
                <?php while ($getposts->have_posts()) : $getposts->the_post(); ?>
                    <div class="block-21 mb-4 d-flex">
                        <a class="img mr-4 rounded" style="background-image: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id(get_the_id()) );?>);"></a>
                        <div class="text">
                            <h3 class="heading"><a href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a></h3>
                            <div class="meta">
                                <div><a href="<?php the_permalink(); ?>"><span class="icon-calendar"></span> <?= get_the_date() ?></a></div>
                                <div><a href="<?php the_permalink(); ?>"><span class="icon-person"></span> <?php the_author(); ?></a></div>
                                <div><a href="<?php the_permalink(); ?>"><span class="icon-chat"></span> <?php echo get_comments_number(); echo __(' bình luận');?> </a></div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; wp_reset_postdata(); ?>
            </div>
            <div class="col-md-6 col-lg-3 pl-lg-5 mb-4 mb-md-0">
                <h2 class="footer-heading">Dịch Vụ</h2>
                <ul class="list-unstyled">
                    <?php
                    $args = array(
                        'type'      => 'post',
                        'child_of'  => 0,
                        'parent'    =>  get_theme_mod('category_service_id')
                    );
                    $categories = get_categories( $args );
                    foreach ( $categories as $category ) : ?>
                        <li>
                            <a class="py-2 d-block" href="<?php echo get_term_link($category->slug, 'category');?>">
                                <?php echo $category->name ; ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="col-md-6 col-lg-3 mb-4 mb-md-0">
                <h2 class="footer-heading">Thông Tin Liên Lạc</h2>
                <div class="block-23 mb-3">
                    <ul>
                        <li><span class="icon fa fa-map"></span><span class="text"><?php echo get_theme_mod('footer_column_4_address'); ?></span>
                        </li>
                        <li><a href="tel: <?php echo get_theme_mod('footer_column_4_phone'); ?>"><span class="icon fa fa-phone"></span><span class="text"><?php echo get_theme_mod('footer_column_4_phone'); ?></span></a>
                        </li>
                        <li><a href="mailto:<?php echo get_theme_mod('footer_column_4_email'); ?>"><span class="icon fa fa-paper-plane"></span><span class="text"><?php echo get_theme_mod('footer_column_4_email'); ?></span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12 text-center">

                <p class="copyright">
                    Copyright &copy;
                    All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by
                    <a target="_blank" href="https://www.facebook.com/khoatran6969">KDT</a></p>
            </div>
        </div>
    </div>
</footer>

<!-- loader -->
<div id="ftco-loader" class="show fullscreen">
    <svg class="circular" width="48px" height="48px">
        <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
        <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#F96D00"/>
    </svg>
</div>

<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v8.0" nonce="OftebOJi"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery-migrate-3.0.1.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/popper.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.easing.1.3.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.waypoints.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.stellar.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.animateNumber.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/bootstrap-datepicker.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.timepicker.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/owl.carousel.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.magnific-popup.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/scrollax.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.colorbox.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/main.js"></script>
<script src="https://kit.fontawesome.com/474c3c3611.js" crossorigin="anonymous"></script>
<?php wp_footer(); ?>
</body>
</html>