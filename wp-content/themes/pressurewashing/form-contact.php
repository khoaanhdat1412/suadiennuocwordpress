<?php
    if ($_POST['send']) {
        $to = get_theme_mod('email_receiver');
        $subject = get_theme_mod('email_subject');
        $name = 'Họ Tên Khách Hàng: ' . $_POST['customerName'] . '<br>';
        $service = 'Dịch Vụ Khách Muốn: ' . $_POST['service'] . '<br>';
        $phone = 'Số Điện Thoại: ' . $_POST['phone'] . '<br>';
        $message = 'Tin Nhắn: ' . $_POST['message'] . '<br>';
        $body = $name . $phone . $service . $message;
        $headers = array('Content-Type: text/html; charset=UTF-8');

        if (wp_mail( $to, $subject, $body, $headers )) {
            echo '<script>alert("Nhân Viên Sẽ Liên Hệ Cho Bạn Trong Thời Gian Sớm Nhất.");</script>';
        }
    }
?>

<form action="#" class="appointment" method="post">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="form-field">
                    <div class="select-wrap">
                        <div class="icon"><span
                                    class="fa fa-chevron-down"></span></div>
                        <select name="service" id="" class="form-control">
                            <?php
                            $args = array(
                                'type' => 'post',
                                'child_of' => 0,
                                'parent' => get_theme_mod('category_service_id')
                            );
                            $categories = get_categories($args);
                            foreach ($categories as $category) : ?>
                                <option value="<?php echo $category->name; ?>"><?php echo $category->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <div class="input-wrap">
                    <div class="icon"><span class="fa fa-user"></span></div>
                    <input name="customerName" type="text" class="form-control"
                           placeholder="Họ tên">
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <div class="input-wrap">
                    <div class="icon"><span class="fa fa-user"></span></div>
                    <input name="phone" type="number" min="10"
                           class="form-control" placeholder="Số điện thoại" required>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <textarea name="message" id="" cols="30" rows="7"
                          class="form-control"
                          placeholder="Tin nhắn"></textarea>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <input name="send" type="submit" value="Gửi Tin Nhắn"
                       class="btn btn-orange py-3 px-4">
            </div>
        </div>
    </div>
</form>