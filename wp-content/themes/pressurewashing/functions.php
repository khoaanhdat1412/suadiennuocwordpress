<?php

function theme_settup() {
    register_nav_menu('topmenu',__( 'Menu chính' ));
    add_theme_support( 'post-thumbnails' );
}
add_action( 'init', 'theme_settup' );

function reason_sections_widgets_init() {
    $args = array(
        'id' => 'reason-section-widget',
        'name' => __( 'Reason Section Homepage Widget Area'),
        'description' => __( 'This is a reason section in homepage.', 'text_domain' ),
        'before_title' => '',
        'after_title' => '',
        'before_widget' => '',
        'after_widget' => '',
    );
    register_sidebar( $args );
}
add_action( 'widgets_init', 'reason_sections_widgets_init' );

function gallery_sections_widgets_init() {
    $args = array(
        'id' => 'gallery-section-widget',
        'name' => __( 'Gallery Section Homepage Widget Area'),
        'description' => __( 'This is a gallery section in homepage.', 'text_domain' ),
        'before_title' => '',
        'after_title' => '',
        'before_widget' => '',
        'after_widget' => '',
    );
    register_sidebar( $args );
}
add_action( 'widgets_init', 'gallery_sections_widgets_init' );

function service_sections_widgets_init() {
    $args = array(
        'id' => 'service-section-widget',
        'name' => __( 'Service Homepage Widget Area'),
        'description' => __( 'This is a service section in homepage.', 'text_domain' ),
        'before_title' => '',
        'after_title' => '',
        'before_widget' => '',
        'after_widget' => '',
    );
    register_sidebar( $args );
}
add_action( 'widgets_init', 'service_sections_widgets_init' );

function address_sections_contact_widgets_init() {
    $args = array(
        'id' => 'address_sections_contact_widget',
        'name' => __( 'Address Section Contact Widget Area'),
        'description' => __( 'This is a address section in contact.', 'text_domain' ),
        'before_title' => '',
        'after_title' => '',
        'before_widget' => '',
        'after_widget' => '',
    );
    register_sidebar( $args );
}
add_action( 'widgets_init', 'address_sections_contact_widgets_init' );

function phone_sections_contact_widgets_init() {
    $args = array(
        'id' => 'phone_sections_contact_widget',
        'name' => __( 'Phone Section Contact Widget Area'),
        'description' => __( 'This is a phone section in contact.', 'text_domain' ),
        'before_title' => '',
        'after_title' => '',
        'before_widget' => '',
        'after_widget' => '',
    );
    register_sidebar( $args );
}
add_action( 'widgets_init', 'phone_sections_contact_widgets_init' );

function email_sections_contact_widgets_init() {
    $args = array(
        'id' => 'email_sections_contact_widget',
        'name' => __( 'Email Section Contact Widget Area'),
        'description' => __( 'This is a email section in contact.', 'text_domain' ),
        'before_title' => '',
        'after_title' => '',
        'before_widget' => '',
        'after_widget' => '',
    );
    register_sidebar( $args );
}
add_action( 'widgets_init', 'email_sections_contact_widgets_init' );

function web_sections_contact_widgets_init() {
    $args = array(
        'id' => 'web_sections_contact_widget',
        'name' => __( 'Web Section Contact Widget Area'),
        'description' => __( 'This is a web section in contact.', 'text_domain' ),
        'before_title' => '',
        'after_title' => '',
        'before_widget' => '',
        'after_widget' => '',
    );
    register_sidebar( $args );
}
add_action( 'widgets_init', 'web_sections_contact_widgets_init' );

function map_sections_widgets_init() {
    $args = array(
        'id' => 'map_sections_widget',
        'name' => __( 'Map Section Widget Area'),
        'description' => __( 'This is a map section.', 'text_domain' ),
        'before_title' => '',
        'after_title' => '',
        'before_widget' => '',
        'after_widget' => '',
    );
    register_sidebar( $args );
}
add_action( 'widgets_init', 'map_sections_widgets_init' );

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Custom Category
 */

/**
 * Config SMTP Send Mail
 */
function send_smtp_email( $phpmailer ) {
    $phpmailer->IsSMTP();
    $phpmailer->Host       = 'smtp.gmail.com';
    $phpmailer->Port       = 465;
    $phpmailer->SMTPAuth   = true;
    $phpmailer->Username   = get_theme_mod('smtp_email'); // Email bạn dùng đăng ký mật khẩu ứng dụng
    $phpmailer->Password   = get_theme_mod('smtp_password'); // Mật khẩu ứng dụng Gmail
    $phpmailer->SMTPSecure = "ssl";
}
add_action( 'phpmailer_init', 'send_smtp_email' );

function add_category_image ( $taxonomy ) {
    ?>
        <div class="form-field term-group">
            <label for="category-image-id"><?php _e('Image', 'hero-theme'); ?></label>
            <input type="hidden" id="category-image-id" name="category-image-id" class="custom_media_url" value="">
            <div id="category-image-wrapper"></div>
            <p>
                <input type="button" class="button button-secondary ct_tax_media_button" id="ct_tax_media_button" name="ct_tax_media_button" value="<?php _e( 'Add Image', 'hero-theme' ); ?>" />
                <input type="button" class="button button-secondary ct_tax_media_remove" id="ct_tax_media_remove" name="ct_tax_media_remove" value="<?php _e( 'Remove Image', 'hero-theme' ); ?>" />
            </p>
        </div>
    <?php
}