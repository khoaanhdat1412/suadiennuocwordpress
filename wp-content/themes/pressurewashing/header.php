<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800&display=swap"
          rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/animate.css">

    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/magnific-popup.css">


    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/jquery.timepicker.css">

    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/flaticon.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/style.css">

    <?php wp_head(); ?>
</head>

<body>
    <div class="wrap">
        <div class="container">
            <div class="row">
                <div class="col-9 col-md-6 d-flex align-items-center">
                    <p class="mb-0 phone pl-md-2">
                        <a href="tel: <?php echo get_theme_mod('top_message_phone');?>" class="mr-2"><span class="fa fa-phone mr-1"></span><?php echo get_theme_mod('top_message_phone');?></a>
                        <a href="#"><span class="fa fa-paper-plane mr-1"></span><?php echo get_theme_mod('top_message_email');?></a>
                    </p>
                </div>
                <div class="col-2 col-md-6 d-flex justify-content-md-end">
                    <div class="social-media">
                        <p class="mb-0 d-flex">
                            <a href="<?php echo get_theme_mod('top_message_facebook');?>" target="_blank" class="d-flex align-items-center justify-content-center"><span
                                        class="fa fa-facebook"><i class="sr-only">Facebook</i></span></a>
                            <a href="<?php echo get_theme_mod('top_message_instagram');?>" target="_blank" class="d-flex align-items-center justify-content-center"><span
                                        class="fa fa-instagram"><i class="sr-only">Instagram</i></span></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        <div class="container">
            <a class="navbar-brand" href="<?php bloginfo('url'); ?>"><span><?php bloginfo('name'); ?></span></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                    aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="fa fa-bars"></span> Menu
            </button>
            <div class="collapse navbar-collapse" id="ftco-nav">
                <?php wp_nav_menu(
                    array(
                        'theme_location' => 'topmenu',
                        'container' => 'false',
                        'menu_id' => 'header-menu',
                        'menu_class' => 'navbar-nav ml-auto'
                    )
                ); ?>
            </div>
        </div>
    </nav>
    <!-- END nav -->

    <?php get_template_part('max-lead'); ?>