<section class="ftco-section gallery-section">
    <div class="container-fluid px-md-4">
        <div class="row">
            <?php dynamic_sidebar( 'gallery-section-widget' ); ?>
        </div>
    </div>
</section>