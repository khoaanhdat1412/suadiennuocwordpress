<?php if (get_theme_mod('service_homepage_style_options') == 1): ?>
    <section class="ftco-section ftco-intro"
             style="background-image: url(<?php echo __(get_theme_mod('service_background_image')) ?>);">
        <div class="container">
            <div class="row justify-content-center pb-5 mb-3">
                <div class="col-md-7 heading-section heading-section-white text-center ftco-animate">
                    <h2><?= __('Bạn Đang Cần Tìm ?') ?></h2>
                </div>
            </div>
            <div class="row">
                <?php
                $args = array(
                    'type'      => 'post',
                    'child_of'  => 0,
                    'parent'    => get_theme_mod('category_service_id')
                );
                $categories = get_categories( $args );
                foreach ( $categories as $category ) : ?>
                    <a href="<?php echo get_term_link($category->slug, 'category');?>" class="col-md-3 d-flex align-self-stretch ftco-animate">
                        <div class="d-block services">
                            <div class="icon d-flex align-items-center justify-content-center">
                                <span class="flaticon-cleaning"></span>
                            </div>
                            <div class="media-body">
                                <h3 class="heading"><?php echo $category->name; ?></h3>
                                <p><?php echo $category->description ?></p>
                            </div>
                        </div>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php elseif (get_theme_mod('service_homepage_style_options') == 2): ?>
    <section class="service-section ftco-section ftco-intro style-two"
             style="background-image: url(<?php echo __(get_theme_mod('service_background_image')) ?>);">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-7 heading-section heading-section-white text-center ftco-animate">
                    <h2><?= __('Bạn Đang Cần Tìm ?') ?></h2>
                </div>
            </div>
            <div class="row row-item-service">
                <?php
                $args = array(
                    'type'      => 'post',
                    'child_of'  => 0,
                    'parent'    => get_theme_mod('category_service_id')
                );
                $categories = get_categories( $args );
                foreach ( $categories as $category ) : ?>
                    <a href="<?php echo get_term_link($category->slug, 'category');?>" class="d-flex align-self-stretch ftco-animate service-item">
                        <div class="d-block services">
                            <div class="image-icon">
                                <?php
                                    $categoryId = $category->term_id;
                                    $image = get_field('category_icon_image', 'category_'. $categoryId);
                                ?>
                                <img src="<?= $image['url'] ?>" alt="">
                            </div>
                            <div class="media-body">
                                <h3 class="heading"><?php echo $category->name; ?></h3>
                                <p><?php echo __('Xem Thêm') ?></p>
                            </div>
                        </div>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif; ?>