<section class="feedback-section ftco-section testimony-section bg-light">
    <div class="overlay"></div>
    <div class="container">
        <div class="row justify-content-center pb-5">
            <div class="col-md-7 heading-section text-center ftco-animate">
                <h2><?= __('Khách Hàng Yêu Thích & Phản Hồi') ?></h2>
            </div>
        </div>
        <div class="row ftco-animate">
            <div class="col-md-12">
                <div class="carousel-testimony owl-carousel">
                    <div class="item">
                        <div class="testimony-wrap d-flex">
                            <div class="user-img"
                                 style="background-image: url(<?php bloginfo('template_directory'); ?>/images/customer-1.jpg)">
                            </div>
                            <div class="text pl-4">
                        <span class="quote d-flex align-items-center justify-content-center">
                          <i class="fa fa-quote-left"></i>
                        </span>
                                <p class="rate">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </p>
                                <p>Dịch vụ ở đây quả thật làm tôi rất hài lòng.</p>
                                <p class="name">Racky Henderson</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimony-wrap d-flex">
                            <div class="user-img"
                                 style="background-image: url(<?php bloginfo('template_directory'); ?>/images/customer-2.jpg)">
                            </div>
                            <div class="text pl-4">
                        <span class="quote d-flex align-items-center justify-content-center">
                          <i class="fa fa-quote-left"></i>
                        </span>
                                <p class="rate">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </p>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia</p>
                                <p class="name">Henry Dee</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimony-wrap d-flex">
                            <div class="user-img"
                                 style="background-image: url(<?php bloginfo('template_directory'); ?>/images/customer-3.jpg)">
                            </div>
                            <div class="text pl-4">
                        <span class="quote d-flex align-items-center justify-content-center">
                          <i class="fa fa-quote-left"></i>
                        </span>
                                <p class="rate">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </p>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia</p>
                                <p class="name">Mark Huff</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimony-wrap d-flex">
                            <div class="user-img"
                                 style="background-image: url(<?php bloginfo('template_directory'); ?>/images/customer-4.jpg)">
                            </div>
                            <div class="text pl-4">
                        <span class="quote d-flex align-items-center justify-content-center">
                          <i class="fa fa-quote-left"></i>
                        </span>
                                <p class="rate">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </p>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia</p>
                                <p class="name">Rodel Golez</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimony-wrap d-flex">
                            <div class="user-img"
                                 style="background-image: url(<?php bloginfo('template_directory'); ?>/images/customer-5.jpg)">
                            </div>
                            <div class="text pl-4">
                        <span class="quote d-flex align-items-center justify-content-center">
                          <i class="fa fa-quote-left"></i>
                        </span>
                                <p class="rate">
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                </p>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia</p>
                                <p class="name">Ken Bosh</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>