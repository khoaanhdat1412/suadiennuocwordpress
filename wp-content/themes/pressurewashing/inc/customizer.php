<?php

function pressurewashing_customize_register($wp_customize) {
    /**
     * Top message
     */
    $wp_customize->add_section("top_message", array(
        'title' => __("Top Message", "pressurewashing"),
        'priority' => 130,
        'description' => __( 'Please choose options for top message in here' ),
    ));

    //Top message Phone
    $wp_customize->add_setting("top_message_phone", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"top_message_phone",array(
        'label' => __("Phone", "pressurewashing"),
        'section' => 'top_message',
        'settings' => 'top_message_phone',
        'type' => 'text',
    )));

    //Top message Email
    $wp_customize->add_setting("top_message_email", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"top_message_email",array(
        'label' => __("Email", "pressurewashing"),
        'section' => 'top_message',
        'settings' => 'top_message_email',
        'type' => 'email',
    )));

    //Top message Social Facebook
    $wp_customize->add_setting("top_message_facebook", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"top_message_facebook",array(
        'label' => __("Link facebook", "pressurewashing"),
        'section' => 'top_message',
        'settings' => 'top_message_facebook',
        'type' => 'text',
    )));

    //Top message Social Instagram
    $wp_customize->add_setting("top_message_instagram", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"top_message_instagram",array(
        'label' => __("Link Instagram", "pressurewashing"),
        'section' => 'top_message',
        'settings' => 'top_message_instagram',
        'type' => 'text',
    )));


    /**
     * Banner Homepage
     */
    $wp_customize->add_section("banner_homepage", array(
        'title' => __("Banner Homepage", "pressurewashing"),
        'priority' => 131,
        'description' => __( 'Please choose options for banner homepage in here' ),
    ));
    //Banner Homepage Text 1
    $wp_customize->add_setting("banner_homepage_text_1", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"banner_homepage_text_1",array(
        'label' => __("Text 1", "pressurewashing"),
        'section' => 'banner_homepage',
        'settings' => 'banner_homepage_text_1',
        'type' => 'text',
    )));

    //Banner Homepage Text 2 - 1
    $wp_customize->add_setting("banner_homepage_text_2_1", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"banner_homepage_text_2_1",array(
        'label' => __("Text 2 - 1 (Yellow)", "pressurewashing"),
        'section' => 'banner_homepage',
        'settings' => 'banner_homepage_text_2_1',
        'type' => 'text',
    )));

    //Banner Homepage Text 2 - 2
    $wp_customize->add_setting("banner_homepage_text_2_2", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"banner_homepage_text_2_2",array(
        'label' => __("Text 2 - 2 (Yellow)", "pressurewashing"),
        'section' => 'banner_homepage',
        'settings' => 'banner_homepage_text_2_2',
        'type' => 'text',
    )));

    //Banner Homepage Text 3
    $wp_customize->add_setting("banner_homepage_text_3", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"banner_homepage_text_3",array(
        'label' => __("Text 3", "pressurewashing"),
        'section' => 'banner_homepage',
        'settings' => 'banner_homepage_text_3',
        'type' => 'text',
    )));

    //Banner Homepage Text Button
    $wp_customize->add_setting("banner_homepage_text_button", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"banner_homepage_text_button",array(
        'label' => __("Text button", "pressurewashing"),
        'section' => 'banner_homepage',
        'settings' => 'banner_homepage_text_button',
        'type' => 'text',
    )));

    //Banner Homepage Link Button
    $wp_customize->add_setting("banner_homepage_link_button", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"banner_homepage_link_button",array(
        'label' => __("Link button", "pressurewashing"),
        'section' => 'banner_homepage',
        'settings' => 'banner_homepage_link_button',
        'type' => 'text',
    )));

    //Banner Homepage Image
    $wp_customize->add_setting("banner_homepage_image", array(
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Upload_Control($wp_customize,'banner_homepage_image',array(
        'label' => __('Image', 'pressurewashing'),
        'section' => 'banner_homepage',
        'settings' => 'banner_homepage_image',
    )));

    /**
     * Counter Homepage
     */
    $wp_customize->add_section("counter_homepage", array(
        'title' => __("Counter Homepage", "pressurewashing"),
        'priority' => 132,
        'description' => __( 'Please choose options for counter homepage in here' ),
    ));

    //Counter Homepage Text Main
    $wp_customize->add_setting("count_homepage_text_main", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"count_homepage_text_main",array(
        'label' => __("Text Main", "pressurewashing"),
        'section' => 'counter_homepage',
        'settings' => 'count_homepage_text_main',
        'type' => 'text',
    )));

    //Counter Homepage Text Button Main
    $wp_customize->add_setting("count_homepage_text_button_main", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"count_homepage_text_button_main",array(
        'label' => __("Text button Main", "pressurewashing"),
        'section' => 'counter_homepage',
        'settings' => 'count_homepage_text_button_main',
        'type' => 'text',
    )));

    //Counter Homepage Phone
    $wp_customize->add_setting("count_homepage_phone", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"count_homepage_phone",array(
        'label' => __("Phone", "pressurewashing"),
        'section' => 'counter_homepage',
        'settings' => 'count_homepage_phone',
        'type' => 'text',
    )));

    //Counter Homepage fake text 1
    $wp_customize->add_setting("count_homepage_fake_text_1", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"count_homepage_fake_text_1",array(
        'label' => __("Fake text 1", "pressurewashing"),
        'section' => 'counter_homepage',
        'settings' => 'count_homepage_fake_text_1',
        'type' => 'text',
    )));

    //Counter Homepage fake number 1
    $wp_customize->add_setting("count_homepage_fake_number_1", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"count_homepage_fake_number_1",array(
        'label' => __("Fake number 1", "pressurewashing"),
        'section' => 'counter_homepage',
        'settings' => 'count_homepage_fake_number_1',
        'type' => 'text',
    )));

    //Counter Homepage fake text 2
    $wp_customize->add_setting("count_homepage_fake_text_2", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"count_homepage_fake_text_2",array(
        'label' => __("Fake text 2", "pressurewashing"),
        'section' => 'counter_homepage',
        'settings' => 'count_homepage_fake_text_2',
        'type' => 'text',
    )));

    //Counter Homepage fake number 2
    $wp_customize->add_setting("count_homepage_fake_number_2", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"count_homepage_fake_number_2",array(
        'label' => __("Fake number 2", "pressurewashing"),
        'section' => 'counter_homepage',
        'settings' => 'count_homepage_fake_number_2',
        'type' => 'text',
    )));

    //Counter Homepage fake text 3
    $wp_customize->add_setting("count_homepage_fake_text_3", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"count_homepage_fake_text_3",array(
        'label' => __("Fake text 3", "pressurewashing"),
        'section' => 'counter_homepage',
        'settings' => 'count_homepage_fake_text_3',
        'type' => 'text',
    )));

    //Counter Homepage fake number 3
    $wp_customize->add_setting("count_homepage_fake_number_3", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"count_homepage_fake_number_3",array(
        'label' => __("Fake number 3", "pressurewashing"),
        'section' => 'counter_homepage',
        'settings' => 'count_homepage_fake_number_3',
        'type' => 'text',
    )));

    //Counter Homepage fake text 4
    $wp_customize->add_setting("count_homepage_fake_text_4", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"count_homepage_fake_text_4",array(
        'label' => __("Fake text 4", "pressurewashing"),
        'section' => 'counter_homepage',
        'settings' => 'count_homepage_fake_text_4',
        'type' => 'text',
    )));

    //Counter Homepage fake number 4
    $wp_customize->add_setting("count_homepage_fake_number_4", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"count_homepage_fake_number_4",array(
        'label' => __("Fake number 4", "pressurewashing"),
        'section' => 'counter_homepage',
        'settings' => 'count_homepage_fake_number_4',
        'type' => 'text',
    )));

    /**
     * Service homepage style
     */
    //Style
    $wp_customize->add_section("service_homepage_style", array(
        'title' => __("Service Homepage Style", "pressurewashing"),
        'priority' => 133,
        'description' => __( 'Please choose options for service homepage style in here' ),
    ));
    $wp_customize->add_setting("service_homepage_style_options", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"service_homepage_style_options",array(
        'label' => __("Style service homepage", "pressurewashing"),
        'section' => 'service_homepage_style',
        'settings' => 'service_homepage_style_options',
        'type' => 'radio',
        'choices' => array(
            1 => __( 'Style 1' ),
            2 => __( 'Style 2' ),
        ),
    )));

    //Background Image
    $wp_customize->add_setting("service_background_image", array(
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Upload_Control($wp_customize,'service_background_image',array(
        'label' => __('Background Image', 'pressurewashing'),
        'section' => 'service_homepage_style',
        'settings' => 'service_background_image',
    )));


    /**
     * Fotter column 1
     */
    $wp_customize->add_section("footer_column_1", array(
        'title' => __("Footer column 1", "pressurewashing"),
        'priority' => 134,
        'description' => __( 'Please choose options for footer column 1 in here' ),
    ));
    // Footer column 1 title
    $wp_customize->add_setting("footer_column_1_title", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"footer_column_1_title",array(
        'label' => __("Footer column 1 title", "pressurewashing"),
        'section' => 'footer_column_1',
        'settings' => 'footer_column_1_title',
        'type' => 'text',
    )));

    //Footer column 1 short description
    $wp_customize->add_setting("footer_column_1_description", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"footer_column_1_description",array(
        'label' => __("Footer column 1 short description", "pressurewashing"),
        'section' => 'footer_column_1',
        'settings' => 'footer_column_1_description',
        'type' => 'textarea',
    )));

    //Footer column 1 link facebook
    $wp_customize->add_setting("footer_column_1_facebook", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"footer_column_1_facebook",array(
        'label' => __("Footer column 1 link Facebook", "pressurewashing"),
        'section' => 'footer_column_1',
        'settings' => 'footer_column_1_facebook',
        'type' => 'text',
    )));

    //Footer column 1 link facebook
    $wp_customize->add_setting("footer_column_1_facebook", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"footer_column_1_facebook",array(
        'label' => __("Footer column 1 link Facebook", "pressurewashing"),
        'section' => 'footer_column_1',
        'settings' => 'footer_column_1_facebook',
        'type' => 'text',
    )));

    //Footer column 1 link instagram
    $wp_customize->add_setting("footer_column_1_instagram", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"footer_column_1_instagram",array(
        'label' => __("Footer column 1 link Instagram", "pressurewashing"),
        'section' => 'footer_column_1',
        'settings' => 'footer_column_1_instagram',
        'type' => 'text',
    )));


    /**
     * Footer column 4
     */
    $wp_customize->add_section("footer_column_4", array(
        'title' => __("Footer column 4", "pressurewashing"),
        'priority' => 135,
        'description' => __( 'Please choose options for footer column 4 in here' ),
    ));

    // Footer column 4 address
    $wp_customize->add_setting("footer_column_4_address", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"footer_column_4_address",array(
        'label' => __("Footer column 4 address", "pressurewashing"),
        'section' => 'footer_column_4',
        'settings' => 'footer_column_4_address',
        'type' => 'textarea',
    )));

    // Footer column 4 phone
    $wp_customize->add_setting("footer_column_4_phone", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"footer_column_4_phone",array(
        'label' => __("Footer column 4 phone", "pressurewashing"),
        'section' => 'footer_column_4',
        'settings' => 'footer_column_4_phone',
        'type' => 'text',
    )));

    // Footer column 4 email
    $wp_customize->add_setting("footer_column_4_email", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"footer_column_4_email",array(
        'label' => __("Footer column 4 email", "pressurewashing"),
        'section' => 'footer_column_4',
        'settings' => 'footer_column_4_email',
        'type' => 'text',
    )));

    /**
     * Config ID Service Category
     */
    $wp_customize->add_section("category_service", array(
        'title' => __("Category Service", "pressurewashing"),
        'priority' => 136,
        'description' => __( 'Please choose category id in here' ),
    ));
    $wp_customize->add_setting("category_service_id", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"category_service_id",array(
        'label' => __("Category service id", "pressurewashing"),
        'section' => 'category_service',
        'settings' => 'category_service_id',
        'type' => 'text',
    )));

    /**
     * Config ID Recruitment Category
     */
    $wp_customize->add_section("category_recruitment", array(
        'title' => __("Category Recruitment", "pressurewashing"),
        'priority' => 137,
        'description' => __( 'Please choose category id in here' ),
    ));
    $wp_customize->add_setting("category_recruitment_id", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"category_recruitment_id",array(
        'label' => __("Category recruitment id", "pressurewashing"),
        'section' => 'category_recruitment',
        'settings' => 'category_recruitment_id',
        'type' => 'text',
    )));


    /**
     * Sidebar
     */
    $wp_customize->add_section("sidebar_config", array(
        'title' => __("Sidebar config", "pressurewashing"),
        'priority' => 138,
        'description' => __( 'Please config sidebar in here' ),
    ));
    $wp_customize->add_setting("number_blog_latest", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"number_blog_latest",array(
        'label' => __("Number of blog lastest", "pressurewashing"),
        'section' => 'sidebar_config',
        'settings' => 'number_blog_latest',
        'type' => 'text',
    )));


    /**
     * Max Lead
     */
    $wp_customize->add_section("maxlead_config", array(
        'title' => __("Maxlead config", "pressurewashing"),
        'priority' => 139,
        'description' => __( 'Please config maxlead in here' ),
    ));

    // Phone number
    $wp_customize->add_setting("maxlead_phone_number", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"maxlead_phone_number",array(
        'label' => __("Phone number", "pressurewashing"),
        'section' => 'maxlead_config',
        'settings' => 'maxlead_phone_number',
        'type' => 'text',
    )));

    // Phone text
    $wp_customize->add_setting("maxlead_phone_text", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"maxlead_phone_text",array(
        'label' => __("Phone text", "pressurewashing"),
        'section' => 'maxlead_config',
        'settings' => 'maxlead_phone_text',
        'type' => 'text',
    )));

    // Zalo number
    $wp_customize->add_setting("maxlead_zalo_number", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"maxlead_zalo_number",array(
        'label' => __("Zalo number", "pressurewashing"),
        'section' => 'maxlead_config',
        'settings' => 'maxlead_zalo_number',
        'type' => 'text',
    )));

    // Zalo text
    $wp_customize->add_setting("maxlead_zalo_text", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"maxlead_zalo_text",array(
        'label' => __("Zalo text", "pressurewashing"),
        'section' => 'maxlead_config',
        'settings' => 'maxlead_zalo_text',
        'type' => 'text',
    )));

    // Facebook link
    $wp_customize->add_setting("maxlead_facebook_link", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"maxlead_facebook_link",array(
        'label' => __("Facebook link", "pressurewashing"),
        'section' => 'maxlead_config',
        'settings' => 'maxlead_facebook_link',
        'type' => 'text',
    )));

    // Facebook text
    $wp_customize->add_setting("maxlead_facebook_text", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"maxlead_facebook_text",array(
        'label' => __("Facebook text", "pressurewashing"),
        'section' => 'maxlead_config',
        'settings' => 'maxlead_facebook_text',
        'type' => 'text',
    )));

    // Map link
    $wp_customize->add_setting("maxlead_map_link", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"maxlead_map_link",array(
        'label' => __("Map link", "pressurewashing"),
        'section' => 'maxlead_config',
        'settings' => 'maxlead_map_link',
        'type' => 'text',
    )));

    // Map text
    $wp_customize->add_setting("maxlead_map_text", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"maxlead_map_text",array(
        'label' => __("Map text", "pressurewashing"),
        'section' => 'maxlead_config',
        'settings' => 'maxlead_map_text',
        'type' => 'text',
    )));

    /**
     * Config email SMTP
     */
    $wp_customize->add_section("smtp_config", array(
        'title' => __("SMTP config", "pressurewashing"),
        'priority' => 140,
        'description' => __( 'Please config SMTP in here' )
    ));
    // Email SMTP
    $wp_customize->add_setting("smtp_email", array(
        'default' => '',
        'transport' => 'postMessage'
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"smtp_email",array(
        'label' => __("SMTP Email", "pressurewashing"),
        'section' => 'smtp_config',
        'settings' => 'smtp_email',
        'type' => 'email'
    )));
    // Password SMTP
    $wp_customize->add_setting("smtp_password", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"smtp_password",array(
        'label' => __("SMTP Password", "pressurewashing"),
        'section' => 'smtp_config',
        'settings' => 'smtp_password',
        'type' => 'password'
    )));

    /**
     * Config Email
     */
    $wp_customize->add_section("email_config", array(
        'title' => __("Email config", "pressurewashing"),
        'priority' => 141,
        'description' => __( 'Please config Email in here' )
    ));
    // Email receiver
    $wp_customize->add_setting("email_receiver", array(
        'default' => '',
        'transport' => 'postMessage'
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"email_receiver",array(
        'label' => __("Email Receiver", "pressurewashing"),
        'section' => 'email_config',
        'settings' => 'email_receiver',
        'type' => 'email'
    )));
    // Email subject
    $wp_customize->add_setting("email_subject", array(
        'default' => '',
        'transport' => 'postMessage'
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"email_subject",array(
        'label' => __("Email Subject", "pressurewashing"),
        'section' => 'email_config',
        'settings' => 'email_subject',
        'type' => 'text'
    )));

    /**
     * Search Page
     */
    //Banner Image
    $wp_customize->add_section("search_image", array(
        'title' => __("Search Image", "pressurewashing"),
        'priority' => 142,
        'description' => __( 'Please choose options for search in here' ),
    ));
    $wp_customize->add_setting("search_banner_background_image", array(
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Upload_Control($wp_customize,'search_banner_background_image',array(
        'label' => __('Search Banner Background Image', 'pressurewashing'),
        'section' => 'search_image',
        'settings' => 'search_banner_background_image',
    )));

    /**
     * Contact Background Image Homepage
     */
    //Background Image
    $wp_customize->add_section("contact_homepage", array(
        'title' => __("Contact Homepage", "pressurewashing"),
        'priority' => 143,
        'description' => __( 'Please choose options for contact homepage in here' ),
    ));
    $wp_customize->add_setting("contact_homepage_background_image", array(
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Upload_Control($wp_customize,'contact_homepage_background_image',array(
        'label' => __('Contact Homepage Background Image', 'pressurewashing'),
        'section' => 'contact_homepage',
        'settings' => 'contact_homepage_background_image',
    )));

}
add_action( 'customize_register', 'pressurewashing_customize_register' );