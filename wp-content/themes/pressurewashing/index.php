<?php get_header(); ?>

<?php get_template_part('banner-homepage'); ?>

<?php get_template_part('reason-section'); ?>

<?php get_template_part('counter-section'); ?>

<?php get_template_part('homepage-service'); ?>

<div class="block-feature-article">
    <?php get_template_part('featured-article-section'); ?>
</div>

<?php get_template_part('homepage-gallery'); ?>

<?php get_template_part('homepage_feedbacks'); ?>

<section class="ftco-appointment ftco-section ftco-no-pt ftco-no-pb img"
             style="background-image: url(<?php echo get_theme_mod('contact_homepage_background_image'); ?>);">
    <div class="overlay"></div>
    <div class="container">
        <div class="row d-md-flex justify-content-center">
            <div class="col-md-12 col-lg-8 half p-3 py-5 pl-lg-5 ftco-animate">
                <h2 class="mb-4"><?= __('Tư Vấn Miễn Phí') ?></h2>
                <?php get_template_part('form-contact'); ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>

<script>
    // window.open('http://toanmy.kdt/',,'GoogleWindow', 'top=10000', 'left=20000', 'width=150, height=150');
    // window.open("http://toanmy.kdt/", "GoogleWindow", "toolbar=yes,scrollbars=yes,resizable=yes,top=10000, left=20000,width=250,height=150");
</script>
