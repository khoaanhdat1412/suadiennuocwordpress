<section class="map ftco-appointment ftco-section img"
         style="background-image: url(<?php bloginfo('template_directory'); ?>/images/map.jpg);">
    <div class="overlay"></div>
    <div class="container">
        <div class="row row-map">
            <?php dynamic_sidebar( 'map_sections_widget' ); ?>
        </div>
    </div>
</section>