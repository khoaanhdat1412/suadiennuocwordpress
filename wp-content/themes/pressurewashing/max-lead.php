<div class="max-lead desktop">
    <div class="phone">
        <a class="icon-call icon" style="background-image: url('<?php bloginfo('template_directory'); ?>/images/icon_call.svg');" href="tel: <?php echo __(get_theme_mod('maxlead_phone_number')); ?>">
            <span class="text"><?php echo __(get_theme_mod('maxlead_phone_text')); ?></span>
        </a>
    </div>

    <div class="zalo">
        <a class="icon-zalo icon" target="_blank" style="background-image: url('<?php bloginfo('template_directory'); ?>/images/icon_zalo.svg');" href="https://zalo.me/<?php echo __(get_theme_mod('maxlead_zalo_number')); ?>">
            <span class="text"><?php echo __(get_theme_mod('maxlead_zalo_text')); ?></span>
        </a>
    </div>

    <div class="messenger">
        <a class="icon-messenger icon" target="_blank" style="background-image: url('<?php bloginfo('template_directory'); ?>/images/icon_messenger.svg');" href="<?php echo __(get_theme_mod('maxlead_facebook_link')); ?>">
            <span class="text"><?php echo __(get_theme_mod('maxlead_facebook_text')); ?></span>
        </a>
    </div>

    <div class="map">
        <a class="icon-map icon" target="_blank" style="background-image: url('<?php bloginfo('template_directory'); ?>/images/icon_map.svg');" href="<?php echo __(get_theme_mod('maxlead_map_link')); ?>">
            <span class="text"><?php echo __(get_theme_mod('maxlead_map_text')); ?></span>
        </a>
    </div>
</div>


<div class="max-lead phone">
    <div class="phone">
        <a class="icon-call icon" style="background-image: url('<?php bloginfo('template_directory'); ?>/images/icon_call_circle.svg');" href="tel: <?php echo __(get_theme_mod('maxlead_phone_number')); ?>">
        </a>
    </div>

    <div class="zalo">
        <a class="icon-zalo icon" target="_blank" style="background-image: url('<?php bloginfo('template_directory'); ?>/images/icon_zalo_circle.svg');" href="https://zalo.me/<?php echo __(get_theme_mod('maxlead_zalo_number')); ?>">
        </a>
    </div>

    <div class="messenger">
        <a class="icon-messenger icon" target="_blank" style="background-image: url('<?php bloginfo('template_directory'); ?>/images/icon_messenger_circle.svg');" href="<?php echo __(get_theme_mod('maxlead_facebook_link')); ?>">
        </a>
    </div>

    <div class="map">
        <a class="icon-map icon" target="_blank" style="background-image: url('<?php bloginfo('template_directory'); ?>/images/icon_map_circle.svg');" href="<?php echo __(get_theme_mod('maxlead_map_link')); ?>">
        </a>
    </div>
</div>