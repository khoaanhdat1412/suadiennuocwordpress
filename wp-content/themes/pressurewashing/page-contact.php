<?php
/*
Template Name: Contact
*/
?>

<?php get_header(); ?>

<section class="hero-wrap hero-wrap-2" style="background-image: url('<?= get_the_post_thumbnail_url(); ?>');"
         data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-end">
            <div class="col-md-9 ftco-animate pb-5">
                <p class="breadcrumbs mb-2"><span class="mr-2"><a href="<?php bloginfo('url'); ?>"><?= __('Trang Chủ') ?> <i
                                class="ion-ios-arrow-forward"></i></a></span> <span><?= single_cat_title( '', false ) ?> <i
                            class="ion-ios-arrow-forward"></i></span></p>
                <h1 class="mb-0 bread"><?= wp_title(  ) ?></h1>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section bg-light">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="wrapper">
                    <div class="row mb-5">
                        <div class="col-md-3">
                            <div class="dbox w-100">
                                <div class="icon d-flex align-items-center justify-content-center">
                                    <span class="fa fa-map-marker"></span>
                                </div>
                                <div class="text">
                                    <?php dynamic_sidebar( 'address_sections_contact_widget' ); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="dbox w-100 text-center">
                                <div class="icon d-flex align-items-center justify-content-center">
                                    <span class="fa fa-phone"></span>
                                </div>
                                <div class="text">
                                    <?php dynamic_sidebar( 'phone_sections_contact_widget' ); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="dbox w-100 text-center">
                                <div class="icon d-flex align-items-center justify-content-center">
                                    <span class="fa fa-paper-plane"></span>
                                </div>
                                <div class="text">
                                    <?php dynamic_sidebar( 'email_sections_contact_widget' ); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="dbox w-100">
                                <div class="icon d-flex align-items-center justify-content-center">
                                    <span class="fa fa-globe"></span>
                                </div>
                                <div class="text">
                                    <?php dynamic_sidebar( 'web_sections_contact_widget' ); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row no-gutters">
                        <div class="col-md-12">
                            <div class="contact-wrap w-100 p-md-5 p-4 bg-secondary">
                                <h3 class="mb-4"><?= __('Nhắn Ngay Cho Chúng Tôi'); ?></h3>
                                <?php get_template_part('form-contact'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_template_part('map-section'); ?>

<?php get_footer(); ?>
