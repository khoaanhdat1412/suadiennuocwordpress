<?php
/**
Template Name: Recruitment Page
 */
?>

<?php get_header(); ?>
<section class="hero-wrap hero-wrap-2" style="background-image: url('<?= get_the_post_thumbnail_url(); ?>');"
         data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-end">
            <div class="col-md-9 ftco-animate pb-5">
                <p class="breadcrumbs mb-2"><span class="mr-2"><a href="<?php bloginfo('url'); ?>"><?= __('Trang Chủ') ?> <i
                                class="ion-ios-arrow-forward"></i></a></span> <span><?= single_cat_title( '', false ) ?> <i
                            class="ion-ios-arrow-forward"></i></span></p>
                <h1 class="mb-0 bread"><?= wp_title(  ) ?></h1>
            </div>
        </div>
    </div>
</section>

<section class="block-recruitment ftco-section ftco-intro bg-light">
    <div class="container">
        <div class="row justify-content-center pb-5 mb-3 row-title">
            <div class="col-md-7 heading-section text-center ftco-animate">
                <h2><?= __('Chúng Tôi Đang Tìm Kiếm') ?></h2>
            </div>
        </div>
        <div class="row row-recruitment">
            <?php
            $args = array(
                'type'      => 'post',
                'child_of'  => 0,
                'parent'    => get_theme_mod('category_recruitment_id')
            );
            $categories = get_categories( $args );
            foreach ( $categories as $category ) : ?>
                <?php
                    $category_id = $category->cat_ID;
                    $image = get_field('category_icon_image', 'category_'. $category_id);
                ?>
                <div class="block-recruitment-item col-12 col-md-4 ftco-animate">
                    <div class="image-category">
                        <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
                        <h3 class="title-category"><?php echo $category->name; ?></h3>
                    </div>
                    <div class="description-category">
                        <span><?php echo $category->description ?></span>
                    </div>
                    <div class="action">
                        <a class="btn btn-orange" href="<?php echo get_term_link($category->slug, 'category');?>"><?= __('Xem Thêm') ?></a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>

<?php get_template_part('counter-section'); ?>

<div class="recruitment">
    <?php get_template_part('homepage-gallery'); ?>
</div>

<?php get_template_part('map-section'); ?>

<?php get_footer(); ?>
