<?php
/**
 Template Name: Service Page
 */
?>

<?php get_header(); ?>
<section class="hero-wrap hero-wrap-2" style="background-image: url('<?= get_the_post_thumbnail_url(); ?>');"
         data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-end">
            <div class="col-md-9 ftco-animate pb-5">
                <p class="breadcrumbs mb-2"><span class="mr-2"><a href="<?php bloginfo('url'); ?>"><?= __('Trang Chủ') ?> <i
                                class="ion-ios-arrow-forward"></i></a></span> <span><?= single_cat_title( '', false ) ?> <i
                            class="ion-ios-arrow-forward"></i></span></p>
                <h1 class="mb-0 bread"><?= wp_title(  ) ?></h1>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section ftco-intro bg-light block-service">
    <div class="container">
        <div class="row justify-content-center mb-3 row-service">
            <div class="col-md-7 heading-section text-center ftco-animate">
                <h2><?= __('Dịch Vụ Của Chúng Tôi') ?></h2>
            </div>
        </div>
        <div class="row">
            <?php
            $args = array(
                'type'      => 'post',
                'child_of'  => 0,
                'parent'    => get_theme_mod('category_service_id')
            );
            $categories = get_categories( $args );
            foreach ( $categories as $category ) : ?>
                <a href="<?php echo get_term_link($category->slug, 'category');?>" class="col-md-3 d-flex align-self-stretch ftco-animate">
                    <div class="d-block services">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="flaticon-cleaning"></span>
                        </div>
                        <div class="media-body">
                            <h3 class="heading"><?php echo $category->name; ?></h3>
                            <p><?php echo $category->description ?></p>
                            <span class="btn-custom d-flex align-items-center justify-content-center">
                                <span class="fa fa-chevron-right"></span>
                                <i class="sr-only">Read more</i>
                            </span>
                        </div>
                    </div>
                </a>
            <?php endforeach; ?>
        </div>
    </div>
</section>

<?php get_template_part('featured-article-section'); ?>

<?php get_template_part('counter-section'); ?>

<div class="mt-3">
    <?php get_template_part('homepage-gallery'); ?>
</div>

<?php get_template_part('map-section'); ?>

<?php get_footer(); ?>
