<?php get_header(); ?>

<section class="hero-wrap hero-wrap-2" style="background-image: url('<?= get_the_post_thumbnail_url(); ?>');"
         data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-end">
            <div class="col-md-9 ftco-animate pb-5">
                <p class="breadcrumbs mb-2"><span class="mr-2"><a href="<?php bloginfo('url'); ?>"><?= __('Trang Chủ') ?> <i
                                class="ion-ios-arrow-forward"></i></a></span> <span><?= single_cat_title( '', false ) ?> <i
                            class="ion-ios-arrow-forward"></i></span></p>
                <h1 class="mb-0 bread"><?= wp_title(  ) ?></h1>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section ftco-degree-bg list-blog">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 ftco-animate">
                <div class="container">
                    <div class="row d-flex">
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
                            the_content();
                        endwhile; else: ?>
                            <p>Sorry, no posts matched your criteria.</p>
                        <?php endif; ?>
                    </div>
                </div>
            </div> <!-- .col-md-8 -->

            <?php get_sidebar(); ?>
        </div>
    </div>
</section> <!-- .section -->

<?php get_template_part('map-section'); ?>

<?php get_footer(); ?>
