<?php if ( is_active_sidebar( 'reason-section-widget' ) ) : ?>
    <section class="ftco-section ftco-no-pt ftco-no-pb">
        <div class="container">
            <div class="row d-flex no-gutters">
                <div class="col-md-12 pl-md-5">
                    <div class="heading-section pt-md-5 text-center">
                        <h2 class="mb-4">Tại Sao Bạn Chọn Chúng Tôi?</h2>
                    </div>
                    <div class="row">
                        <?php dynamic_sidebar( 'reason-section-widget' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>


<!--<div class="col-md-4 services-2 w-100 d-flex">-->
<!--    <div class="icon d-flex align-items-center justify-content-center"><span-->
<!--                class="flaticon-confetti"></span></div>-->
<!--    <div class="text pl-3">-->
<!--        <h4>Giá Cả Thấp Nhất Thị Trường</h4>-->
<!--        <p>Far far away, behind the word mountains, far from the countries.</p>-->
<!--    </div>-->
<!--</div>-->
<!--<div class="col-md-4 services-2 w-100 d-flex">-->
<!--    <div class="icon d-flex align-items-center justify-content-center"><span-->
<!--                class="flaticon-consult"></span></div>-->
<!--    <div class="text pl-3">-->
<!--        <h4>Nhân Viên Chuyên Nghiệp &amp; Giàu Kinh Nghiệm</h4>-->
<!--        <p>Far far away, behind the word mountains, far from the countries.</p>-->
<!--    </div>-->
<!--</div>-->
<!--<div class="col-md-4 services-2 w-100 d-flex">-->
<!--    <div class="icon d-flex align-items-center justify-content-center"><span-->
<!--                class="flaticon-winner"></span></div>-->
<!--    <div class="text pl-3">-->
<!--        <h4>Dịch Vụ Chất Lượng Cao &amp; Đáng Tin Cậy</h4>-->
<!--        <p>Far far away, behind the word mountains, far from the countries.</p>-->
<!--    </div>-->
<!--</div>-->
<!--<div class="col-md-4 services-2 w-100 d-flex">-->
<!--    <div class="icon d-flex align-items-center justify-content-center"><span-->
<!--                class="flaticon-technical"></span></div>-->
<!--    <div class="text pl-3">-->
<!--        <h4>Dịch Vụ Tư Vấn 24/7</h4>-->
<!--        <p>Far far away, behind the word mountains, far from the countries.</p>-->
<!--    </div>-->
<!--</div>-->
<!--<div class="col-md-4 services-2 w-100 d-flex">-->
<!--    <div class="icon d-flex align-items-center justify-content-center"><span-->
<!--                class="flaticon-technical"></span></div>-->
<!--    <div class="text pl-3">-->
<!--        <h4>Nhanh Chóng & Thân Thiện</h4>-->
<!--        <p>Far far away, behind the word mountains, far from the countries.</p>-->
<!--    </div>-->
<!--</div>-->
<!--<div class="col-md-4 services-2 w-100 d-flex">-->
<!--    <div class="icon d-flex align-items-center justify-content-center"><span-->
<!--                class="flaticon-technical"></span></div>-->
<!--    <div class="text pl-3">-->
<!--        <h4>Làm Việc Kể Cả Ngày Lễ</h4>-->
<!--        <p>Far far away, behind the word mountains, far from the countries.</p>-->
<!--    </div>-->
<!--</div>-->