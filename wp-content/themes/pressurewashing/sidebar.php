<div class="col-lg-4 sidebar pl-lg-5 ftco-animate">
    <div class="sidebar-box">
        <form id="myformSearch" action="<?php bloginfo('url'); ?>/" method="GET" role="form" class="search-form">
            <div class="form-group">
                <span class="fa fa-search icon-search" onclick="document.getElementById('myformSearch').submit()"></span>

                <input type="text" name="s" class="form-control" required="required" placeholder="<?= __('Tìm kiếm dịch vụ bạn muốn....') ?>">
            </div>
        </form>
    </div>
    <div class="sidebar-box ftco-animate">
        <div class="categories">
            <h3><?= __('Dịch Vụ'); ?></h3>

            <?php
            $args = array(
                'type'      => 'post',
                'child_of'  => 0,
                'parent'    => get_theme_mod('category_service_id')
            );
            $categories = get_categories( $args );
            foreach ( $categories as $category ) : ?>
                <li>
                    <a href="<?php echo get_term_link($category->slug, 'category');?>">
                        <?php echo $category->name ; ?> (<?php echo $category->count ; ?>)
                        <span class="fa fa-chevron-right"></span>
                    </a>
                </li>
            <?php endforeach; ?>
        </div>
    </div>

    <div class="sidebar-box ftco-animate">
        <h3><?= __('Bài Viết Mới Nhất'); ?></h3>


        <?php
        $args = array(
            'post_status' => 'publish', // Chỉ lấy những bài viết được publish
            'post_type' => 'post', // Lấy những bài viết thuộc post, nếu lấy những bài trong 'trang' thì để là page
            'showposts' => get_theme_mod('number_blog_latest') - 1 // số lượng bài viết
        );
        ?>
        <?php $getposts = new WP_query($args); ?>
        <?php global $wp_query; $wp_query->in_the_loop = true; ?>
        <?php while ($getposts->have_posts()) : $getposts->the_post(); ?>
            <div class="blog-entry block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id(get_the_id()) );?>);"></a>
                <div class="text">
                    <h3 class="heading"><a href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a></h3>
                    <div class="meta">
                        <div><a href="<?php the_permalink(); ?>"><span class="icon-calendar"></span> <?= get_the_date() ?></a></div>
                        <div><a href="<?php the_permalink(); ?>"><span class="icon-person"></span> <?php the_author(); ?></a></div>
                        <div><a href="<?php the_permalink(); ?>"><span class="icon-chat"></span> <?php echo get_comments_number(); echo __(' bình luận');?> </a></div>
                    </div>
                </div>
            </div>
        <?php endwhile; wp_reset_postdata(); ?>
    </div>

    <div class="sidebar-box ftco-animate">
        <h3><?= __('FanPage'); ?></h3>
        <div class="fb-page" data-href="https://www.facebook.com/kythuattoanmy" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/kythuattoanmy" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/kythuattoanmy">Công Ty TNHH Kỹ Thuật Toàn Mỹ</a></blockquote></div>
    </div>
</div>