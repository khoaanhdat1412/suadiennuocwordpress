<?php get_header(); ?>

<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
        <?php $image = get_field('post_banner_image', get_the_id()); ?>
        <section class="hero-wrap hero-wrap-2"
                 style="background-image: url('<?= $image['url'] ?>');"
                 data-stellar-background-ratio="0.5">
            <div class="overlay"></div>
            <div class="container">
                <div class="row no-gutters slider-text align-items-end">
                    <div class="col-md-9 ftco-animate pb-5">
                        <p class="breadcrumbs mb-2"><span class="mr-2"><a
                                        href="<?php bloginfo('url'); ?>"><?= __('Trang Chủ') ?> <i
                                            class="ion-ios-arrow-forward"></i></a></span>
                            <span><?= single_cat_title('', false) ?> <i
                                        class="ion-ios-arrow-forward"></i></span></p>
                        <h1 class="mb-0 bread"><?= the_title() ?></h1>
                    </div>
                </div>
            </div>
        </section>

        <section class="ftco-section ftco-degree-bg list-blog">
            <div class="container">
                <div class="row">
                    <div class="block-blog col-lg-8 ftco-animate">
                        <p>
                            <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id(get_the_id())); ?>" alt=""
                                 class="img-fluid">
                        </p>

                        <?= the_content() ?>

                        <div class="tag-widget post-tag-container mb-5 mt-5">
                            <div class="tagcloud">
                                <?php $posttags = get_the_tags(); ?>
                                <?php if ($posttags): ?>
                                    <?php foreach ($posttags as $tag): ?>
                                        <a href="#" class="tag-cloud-link"><?= $tag->name ?></a>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="social-share">
                            <h5><?= __('Chia sẻ bài viết: ') ?></h5>
                            <a title="Facebook" href="https://www.facebook.com/sharer.php?u=<?= the_permalink() ?>" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=500px'); return false;">
                                <i class="fab fa-facebook-square"></i>
                            </a>
                            <a title="Instagram" class="icon-instagram" href="https://www.instagram.com/cokhithongnhat" target="_blank">
                                <i class="fab fa-instagram"></i>
                            </a>
                            <a title="LinkedIn" class="icon-linkedin" href="https://www.linkedin.com/sharing/share-offsite/?url=<?= the_permalink() ?>" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=500px'); return false;">
                                <i class="fab fa-linkedin"></i>
                            </a>
                            <a title="Twitter" class="icon-twitter" href="https://www.twitter.com/intent/tweet?url=<?= the_permalink() ?>" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=500px'); return false;">
                                <i class="fab fa-twitter-square"></i>
                            </a>
                            <a title="Pinterest" class="icon-pinterest" href="https://pinterest.com/pin/create/button/?url=<?= the_permalink() ?>&media=<?php echo wp_get_attachment_url(get_post_thumbnail_id(get_the_id())); ?>&description=<?php the_title(); ?>" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=500px'); return false;">
                                <i class="fab fa-pinterest-square"></i>
                            </a>
                        </div>

                        <div class="mt-4 fb-like" data-href="<?= the_permalink() ?>" data-width="400"
                             data-layout="standard" data-action="like" data-size="small" data-share="true"></div>
                        <div class="mt-2 fb-comments" data-href="<?= the_permalink() ?>" data-numposts="5" data-width="100%"></div>

                        <div class="mt-4">
                            <section class="ftco-appointment ftco-section ftco-no-pt ftco-no-pb img">
                                <div class="overlay"></div>
                                <div class="container">
                                    <div class="row d-md-flex justify-content-center">
                                        <div class="col-md-12 col-lg-8 half p-3 py-5 pl-lg-5 ftco-animate">
                                            <h2 class="mb-4"><?= __('Tư Vấn Miễn Phí') ?></h2>
                                            <?php get_template_part('form-contact'); ?>
                                        </div>
                                    </div>
                                </div>
                            </section>

                        </div>
                    </div> <!-- .col-md-8 -->

                    <?php get_sidebar(); ?>
                </div>
            </div>
        </section> <!-- .section -->
    <?php endwhile; ?>
<?php endif; ?>

<?php get_template_part('map-section'); ?>

<?php get_footer(); ?>